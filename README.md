Class Assignment - CNIT 255 (Object-Oriented Programming Introduction) - Purdue University, 2015 Fall

This project was developed for the CNIT 255 (Object-Oriented Programming Introduction) class at Purdue University on the Fall of 2014.
It is an desktop application developed using .NET on Visual Studio for the Purdue Polytechinic's 3D printing lab. It controls the lab supplies and students quota.
This repository doesn't contain the final version of the code, but I decided to keep it here for reference.
